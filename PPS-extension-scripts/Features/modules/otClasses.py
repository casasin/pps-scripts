#

import random


class otClass(object):
    __doc__ = """An opentype features class"""

    def __init__(self, name="default", glyphs=[], parentFont=None):
        # super(otClass, self).__init__()
        self.name = name
        self.glyphs = glyphs
        self.parentFont = parentFont
        self.feaCode = self.otClassCode()

    # def isList(self, glyphs):
    #     __doc__ = """Be sure self.glyphs is a list"""
    #     try type(self.glyphs) == list:
    #         pass
    #     except:
    #         self.glyphs = list(filter(None, re.split(' |/', self.glyphs)))

    def otClassCode(self):
        codestr = f"@{self.name} = [{' '.join(self.glyphs)}];"
        return codestr


class otClassSplitter(object):
    __doc__ = """split a class in a number of ot classes"""

    def __init__(self, otClass, nsplit=4, mix=False):
        # super(otClassSplitter, self).__init__()
        self.otClass = otClass
        self.nsplit = nsplit
        self.mix = mix

    def splitBaseClass(self):
        __doc__ = """Split a class in n elements of similar length if can't be equally."""
        # keep original source untouched
        glyphs = self.otClass.glyphs[:]
        if self.mix is True:
            random.shuffle(glyphs)
        k, m = divmod(len(glyphs), self.nsplit)
        return list(glyphs[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(self.nsplit))

    def makeSplitClasses(self):
        self.splitClasses = []
        splits = self.splitBaseClass()
        for i in range(len(splits)):
            n = i + 1
            splitClassName = f"{self.otClass.name}_{n}"
            splitGlyphs = splits[i]
            otSplitClass = otClass(name=splitClassName, glyphs=splitGlyphs)
            self.splitClasses.append(otSplitClass)
        return self.splitClasses


# if __name__ == "__main__":
#     glyphNames = "a b c d e f g h i j k l m n o p q r s t u v w x y z".split()
#     baseClass = otClass(name='dflt', glyphs=glyphNames)
#     splitter = otClassSplitter(baseClass)
#     splitter.mix = False
#     splitter.makeSplitClasses()
#     for sc in splitter.splitClasses:
#         print(sc.feaCode)
