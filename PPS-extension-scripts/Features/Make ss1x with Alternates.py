# MenuTitle: Make ss1x with Alternates

__doc__ = """\
    Adds stylistic sets for each numbered alternates as ss1x for development purposes.
    So:
    - ss11 will contain all *.alt1 glyphs.
    - ss12 will contain all *.alt2 glyphs.
    - and so on …
"""


def substitutionCode(glyphNames, name=""):
    code = []
    if name:
        code = [f'featureNames {{\n\tname "{name}";\n}};\n']
    for tgt in glyphNames:
        src = '.'.join(tgt.split('.')[:-1])
        code.append(f"sub {src} by {tgt};")
    return "\n".join(code)


def makeAlternatesDict(glyphs, n=10):
    alternateGlyphsDict = {}
    for x in range(1, n):
        alternateGlyphsDict[x] = [glyph.name for glyph in glyphs
                                  if ".alt" in glyph.name
                                  if glyph.name[-1] == str(x)
                                  ]
    return alternateGlyphsDict


def addFeaturesForAlternates(font):
    alternateGlyphsDict = makeAlternatesDict(font.glyphs)

    for nAlt, altGlyphs in alternateGlyphsDict.items():
        feaName = f"ss1{nAlt}"
        if altGlyphs:
            del font.features[feaName]
            font.features.append(GSFeature(feaName))
            # font.features[feaName].labels = f"Alternates {nAlt}"
            font.features[feaName].code = substitutionCode(altGlyphs, name=f"Alternates {nAlt}")


if __name__ == '__main__':
    Glyphs.clearLog()

    current_font = Glyphs.font
    addFeaturesForAlternates(current_font)

    print("Added Stylistic Sets features for alternate glyphs numbered (for development purposes).")
    Glyphs.showMacroWindow()
