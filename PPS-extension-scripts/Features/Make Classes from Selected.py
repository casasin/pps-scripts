# MenuTitle: Make Classes from Selected

__doc__ = """
    Select some glyphs **without the alternate extension** in the font window and will create default and alternate classes from the selected.
    Used variables:
    - split the base class in n classes: 4
    - each class for n alternates: 6
    - alternate prefix: "alt" (the same in glyph names)
    - a separator: "_"
"""

from modules.otClasses import *

nsplit = 4
nAlternates = 6
alt_prefix = "alt"
sep = "_"

Glyphs.clearLog()

cf = Glyphs.font
selectedGlyphNames = [g.name for g in cf.glyphs if g.selected]

# all classes code:
defaultClasses = ["# DEFAULT CLASSES:"]
altClasses = ["\n\n# ALTERNATE CLASSES:"]
codeString = ""

# Joined classes
allDefaultClass = otClass(name='dflt_ALL')

baseClass = otClass(name='dflt', glyphs=selectedGlyphNames)
splitter = otClassSplitter(baseClass, nsplit=nsplit)
splitter.mix = True
dflt_splitClasses = splitter.makeSplitClasses()
allDefaultClass.glyphs = [f"@{sc.name}" for sc in dflt_splitClasses]

for sc in dflt_splitClasses:
    defaultClasses.append(sc.feaCode)
    altClasses.append("#")
    # make alt classes from default
    for alt in range(nAlternates):
        alt += 1
        altClassName = sc.name.replace(baseClass.name, f"{alt_prefix}{alt}")
        altGlyphs = [f"{gname}.{alt_prefix}{alt}" for gname in sc.glyphs]
        altClass = otClass(name=altClassName, glyphs=altGlyphs)
        altClasses.append(altClass.feaCode)

# make allAltClasses (dirty)
allAltClasses = []
for alt in range(nAlternates+1)[1:]:
    altAllClassName = f"{alt_prefix}{alt}_ALL"
    members = []
    for n in range(nsplit+1)[1:]:
        members.append(f"@{altAllClassName[:-4]}{sep}{n}")
    altClassesClass = otClass(name=altAllClassName, glyphs=members)
    allAltClasses.append(altClassesClass)


codeString += '\n'.join(defaultClasses)
codeString += '\n'.join(altClasses)

# print(codeString)
# print("#")
# print(allDefaultClass.otClassCode())
# for c in allAltClasses:
#     print(c.otClassCode())

Glyphs.showMacroWindow()
