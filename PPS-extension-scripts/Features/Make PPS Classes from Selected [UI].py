# MenuTitle: Make PPS Classes from Selected [UI]

__doc__ = """
    Select some glyphs **without the alternate extension** in the font window and will create default and alternate classes from the selected.
    Used variables:
    - split the base class in n classes: 4
    - each class for n alternates: 6
    - alternate prefix: "alt" (the same in glyph names)
    - a separator: "_"
"""

import subprocess
from modules.otClasses import *
from vanilla import *


class MakePPSClassesWindow(object):
    __doc__ = """UI for making PPS classes split for further `calt` pseudorandomization"""

    def __init__(self):
        super(MakePPSClassesWindow, self).__init__()

        self.font = Glyphs.font

        width, height = 200, 260
        gutter = 10

        self.w = Window((width, height), "Make PPS Classes",)
        # maxSize=(250, height), minSize=(width, height))

        self.w.labelAlts = TextBox(
            (gutter + 2, gutter + 2, -20, 22), "n Alternates:")
        self.w.editAlts = EditText((100, gutter, -70, 22), "6",)

        self.w.labelSplit = TextBox(
            (gutter + 2, 40, -20, 22), "n Splits:")
        self.w.editSplits = EditText((100, 36, -70, 22), "4",)

        self.w.labelPrefix = TextBox(
            (gutter + 2, 68, -20, 22), "alt @Prefix:")
        self.w.editPrefix = EditText((100, 65, -gutter, 22), "alt",)

        self.w.labelSeparator = TextBox(
            (gutter + 2, 96, -20, 22), "Separator:")
        self.w.editSeparator = EditText((100, 94, -70, 22), "_",)

        self.w.line = HorizontalLine((10, 130, -gutter, 1))
        self.w.printCheckbox = CheckBox((14, 138, -gutter, 20), "Print @classes to Output",
                                        value=True, sizeStyle="small")
        self.w.copyCheckbox = CheckBox((14, 158, -gutter, 20), "Copy to Clipboard",
                                       value=True, sizeStyle="small")
        self.w.line2 = HorizontalLine((10, 184, -gutter, 1))

        self.w.warningLabel = TextBox((gutter, 200, 0, -gutter), "REPORTER HERE",
                                      sizeStyle="mini")

        self.w.makeButton = Button((gutter, height - gutter * 3, - gutter, 20), f"Make Split {self.w.editPrefix.get()} @classes", sizeStyle="small",
            callback=self.makeCallback)

        self.reportSelected(self.font)

        self.w.setDefaultButton(self.w.makeButton)
        self.w.bind("became key", callback=self.keyCallback)
        self.w.bind("resigned key", callback=self.keyCallback)

        self.w.open()

    def keyCallback(self, sender):
        # Update current font
        # self.font = Glyphs.font
        if self.font:
            self.reportSelected(self.font)
        else:
            self.w.warningLabel.set(f"🚫 No font open")

    def reportSelected(self, gfont):
        if gfont:
            # report the number of glyphs selected in self.font (probably in more places)
            sel = len(gfont.selection)
            if sel < 20:
                self.w.warningLabel.set(f"⚠️ Selected {sel} glyphs")
            elif 20 < sel < 40:
                self.w.warningLabel.set(f"⁉️ Selected {sel} glyphs")
            else:
                self.w.warningLabel.set(f"✅ Selected {sel} glyphs")
        else:
            self.w.warningLabel.set(f"🚫 No font open")

    def writeToClipboard(self, output):
        process = subprocess.Popen(
            'pbcopy', env={'LANG': 'en_US.UTF-8'}, stdin=subprocess.PIPE)
        process.communicate(output.encode('utf-8'))

    def makeCallback(self, sender):
        cf = self.font
        if not self.font:
            return

        nsplit = int(self.w.editSplits.get())
        nAlternates = int(self.w.editAlts.get())
        alt_prefix = self.w.editPrefix.get()
        sep = self.w.editSeparator.get()

        Glyphs.clearLog()

        selectedGlyphNames = [g.name for g in cf.glyphs if g.selected]

        # all classes code:
        defaultClasses = ["# DEFAULT CLASSES:"]
        altClasses = ["\n\n# ALTERNATE CLASSES:"]
        codeString = ""
        code = []

        # Joined classes
        allDefaultClass = otClass(name='dflt_ALL')

        baseClass = otClass(name='dflt', glyphs=selectedGlyphNames)
        splitter = otClassSplitter(baseClass, nsplit=nsplit)
        splitter.mix = True
        dflt_splitClasses = splitter.makeSplitClasses()
        allDefaultClass.glyphs = [f"@{sc.name}" for sc in dflt_splitClasses]

        for sc in dflt_splitClasses:
            defaultClasses.append(sc.feaCode)
            altClasses.append("#")
            # make alt classes from default
            for alt in range(nAlternates):
                alt += 1
                altClassName = sc.name.replace(baseClass.name, f"{alt_prefix}{alt}")
                altGlyphs = [f"{gname}.{alt_prefix}{alt}" for gname in sc.glyphs]
                altClass = otClass(name=altClassName, glyphs=altGlyphs)
                altClasses.append(altClass.feaCode)

        # make allAltClasses (dirty)
        allAltClasses = []
        for alt in range(nAlternates+1)[1:]:
            altAllClassName = f"{alt_prefix}{alt}_ALL"
            members = []
            for n in range(nsplit+1)[1:]:
                members.append(f"@{altAllClassName[:-4]}{sep}{n}")
            altClassesClass = otClass(name=altAllClassName, glyphs=members)
            allAltClasses.append(altClassesClass)


        code.append("\n".join(defaultClasses))
        code.append("\n".join(altClasses))
        # codeString += '\n'.join(defaultClasses)
        # codeString += '\n'.join(altClasses)

        # print(codeString)
        # # print("#")
        # print(allDefaultClass.otClassCode())
        code.append( "#ALL\n" + allDefaultClass.otClassCode())
        for c in allAltClasses:
            # print(c.otClassCode())
            code.append(c.otClassCode())

        # copy to clipboard
        if self.w.copyCheckbox.get():
            self.writeToClipboard("\n".join(code))

        Glyphs.showMacroWindow()
        print("\n".join(code))



# # ================================

if __name__ == '__main__':

    MakePPSClassesWindow()
