# MenuTitle: Sort Alternates with Default

__doc__ = """Sort current font glyphOrder appending alternates to base characters if there are several alternates and named '*.altn' being n a digit or more.
If there's any glyph out of the glyphOrder it will add it in place as is."""

import re

cf = Glyphs.font
go = cf.customParameters["glyphOrder"]
go = [g.name for g in cf.glyphs]

# any glyphname wich ends with ".alt" *and* one or more digits
pattern = r".+?.alt+\d$"
# List with glyphs without the pattern in their name
default = [gname for gname in go if re.match(pattern, gname) is None]
# insert their alternates
new_go = []
alt_pattern = r".alt+\d?$"
for gname in default:
    new_go.append(gname)
    alternates = sorted([x for x in go if re.match(f"{gname}{alt_pattern}", x) is not None])
    if alternates:
        new_go += alternates

print(new_go)

cf.customParameters["glyphOrder"] = new_go

print("""Sorted font.glyphOrder with base glyphs and their alternates together.
Kept '.alt' no numbered alternate in place.""")
