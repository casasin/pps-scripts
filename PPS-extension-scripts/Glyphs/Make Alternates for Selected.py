# MenuTitle: Make Alternates for Selected

__doc__ = """\
    Creates new alternate glyphs for selected glyphs in the Font Window.
    Usage:
        - Select the glyphs you want to create alternates.
        (…)
"""

from vanilla import *


class MakeAlternatesWindow(object):
    __doc__ = """UI for building alternate glyphs for selected glyphs."""

    def __init__(self):
        super(MakeAlternatesWindow, self).__init__()
        self.font = Glyphs.font
        self.report = []
        if not self.font:
            Message("You need to open a font first.",
                    title="⚠️ ⚠️ ⚠️", OKButton=None)
            return
        width, height = 240, 200
        gutter = 10
        self.w = FloatingWindow((width, height), "Make Alternates")

        self.w.labelAlts = TextBox(
            (gutter + 2, gutter + 2, -20, 22), "Number of Alternates:")
        self.w.editAlts = EditText((-80, gutter, -20, 22), "3")

        self.w.labelPrefix = TextBox((-133, 42, -20, 22), "Suffix:")
        self.w.editSuffix = EditText((-80, 40, - 20, 22), "alt")

        self.w.line = HorizontalLine((gutter + 2, 77, -10, 1))

        self.w.insertInGlyphOrder = CheckBox(
            (gutter + 2, 90, -10, 22), "Insert in glyphorder", value=False)
        self.w.helpInsertButton = HelpButton((155, 90, 21, 20),
                                   callback=None)
        self.w.helpInsertButton.setToolTip("Base glyphs *must* be already in the glyphOrder.")
        
        self.w.appendToExisting = CheckBox(
            (gutter + 2, 115, -10, 22), "Append if existing", value=False)
        self.w.helpAppendButton = HelpButton((155, 115, 21, 20),
                                   callback=None)
        self.w.helpAppendButton.setToolTip("Will append n alternates to the last existing.")

        self.w.makeButton = Button(
            (10, height - gutter * 4, -10, 0), "Make Alternates", self.makeAlternatesCallback)

        self.w.insertInGlyphOrder.enable(False)
        self.w.appendToExisting.enable(False)

        self.w.setDefaultButton(self.w.makeButton)
        self.w.open()

    def updateFontCallback(self, sender):
        print('became key!')

    def buildGlyphNames(self, n):
        # update current font just in case
        self.font = Glyphs.font
        existingGlyphNames = [gn.name for gn in self.font.glyphs]
        selection = [gn.name for gn in self.font.selection]
        glyphNames = []

        if not selection:
            Message("You need to select at least one glyph to make alternates from!",
                    title="⚠️ ⚠️ ⚠️", OKButton=None)


        self.suffix = self.w.editSuffix.get()

        if self.suffix[0] == ".":
            self.suffix = self.suffix[1:]

        for gname in selection:
            for i in range(n):
                alt = i + 1
                new_gname = f"{gname}.{self.suffix}{str(alt)}"

                if new_gname not in existingGlyphNames:
                    glyphNames.append(new_gname)
                else:
                    self.report.append(f"⚠️ Already exists: /{new_gname}")

        return glyphNames

    def makeAlternatesCallback(self, sender):
        insert = self.w.insertInGlyphOrder.get()
        nAlts = self.w.editAlts.get()

        try:
            n = int(self.w.editAlts.get())
        except ValueError:
            n = None
            Message("⚠️ Number of Alternates must be integer!")

        if n:
            addGlyphs = self.buildGlyphNames(n)

            for gname in addGlyphs:
                self.font.glyphs.append(GSGlyph(gname))

        print("\n".join(self.report))
        print(f"\n{'=' * 20}\n DONE!")
        Glyphs.showMacroWindow()

MakeAlternatesWindow()
