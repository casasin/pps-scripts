# MenuTitle: Select Glyphs Without Alternates

__doc__ = """
    Select glyphs which doesn't have any alternate.
"""


import re

cf = Glyphs.font
go = [g.name for g in cf.glyphs]

# any glyphname wich ends with ".alt" *and* one or more digits
pattern = r".+?.alt+\d$"
# List with glyphs without the pattern in their name
default = [gname for gname in go if re.match(pattern, gname) is None]
# insert their alternates
new_go = []
alt_pattern = r".alt+\d?$"
for gname in default:
    alternates = sorted([x for x in go if re.match(f"{gname}{alt_pattern}", x) is not None])
    if not alternates and cf.glyphs[gname].export is True:
        cf.glyphs[gname].selected = True
