# MenuTitle: Select Glyphs with n Alternates [UI]

__doc__ = """\
    Select glyphs with a given number of alternates UI.
"""

from vanilla import *


class SelectAlternatesWindow(object):
    __doc__ = """UI for selecting glyphs with a given number of alternates."""

    def __init__(self):
        super(SelectAlternatesWindow, self).__init__()
        self.font = Glyphs.font
        self.report = []
        if not self.font:
            Message("You need to open a font first.",
                    title="⚠️ ⚠️ ⚠️", OKButton=None)
            return
        width, height = 240, 200
        gutter = 10
        self.w = FloatingWindow((width, height), "Select by Alternates number")

        self.w.labelAlts = TextBox(
            (gutter + 2, gutter + 2, -20, 22), "Number of Alternates:")
        self.w.editAlts = EditText((-80, gutter, -20, 22), "6",
                                   callback=self.updateTitlesCallback)

        self.w.labelPrefix = TextBox((-133, 42, -20, 22), "Suffix:")
        self.w.editSuffix = EditText((-80, 40, - 20, 22), ".alt")

        self.w.line = HorizontalLine((gutter + 2, 77, -10, 1))

        self.w.radioGroup = RadioGroup((10, 90, -10, 40),
                                       [f"Strict number of alternates",
                                        f"At least those alternates"],
                                       # callback=self.radioGroupCallback)
                                       )
        self.w.radioGroup.set(0)

        self.w.useSelectionCheckBox = CheckBox((14, 140, -10, 20), "Use selected glyphs",
         value=False, sizeStyle="small")

        # self.w.insertInGlyphOrder = CheckBox(
        #     (gutter + 2, 90, -10, 22), "Insert in glyphorder", value=False)
        # self.w.helpInsertButton = HelpButton((155, 90, 21, 20),
        #                            callback=None)
        # self.w.helpInsertButton.setToolTip("Base glyphs *must* be already in the glyphOrder.")

        # self.w.appendToExisting = CheckBox(
        #     (gutter + 2, 115, -10, 22), "Append if existing", value=False)
        # self.w.helpAppendButton = HelpButton((155, 115, 21, 20),
        #                            callback=None)
        # self.w.helpAppendButton.setToolTip("Will append n alternates to the last existing.")

        self.w.selectButton = Button(
            (10, height - gutter * 4, -10, 0), f"Select have {self.w.editAlts.get()} Alternates", self.selectAlternatesCallback)

        # self.w.insertInGlyphOrder.enable(False)
        # self.w.appendToExisting.enable(False)

        self.w.setDefaultButton(self.w.selectButton)
        self.w.open()

    def updateTitlesCallback(self, sender):
        self.w.selectButton.setTitle(f"Select have {self.w.editAlts.get()} Alternates")
        print(dir(self.w.radioGroup))

    def makeAlternatesDict(self, glyphsList):
        suffix = self.w.editSuffix.get()
        if suffix[0] != ".":
            suffix = "." + suffix

        self.alternatesDict = {}
        glyphNames = [g.name for g in glyphsList]
        for gname in glyphNames:
            if suffix not in gname:
                self.alternatesDict[gname] = [
                    n for n in glyphNames if n.startswith(gname + suffix)]

    def selectAlternatesCallback(self, sender):
        # update current font just in case
        self.font = Glyphs.font

        if self.w.useSelectionCheckBox.get() == 0:
            self.makeAlternatesDict(self.font.glyphs)
        else:
            self.makeAlternatesDict(self.font.selection)

        selectGlyphs = []
        if self.w.radioGroup.get() == 0:
            strict = True
        else:
            strict = False

        try:
            n = int(self.w.editAlts.get())
        except ValueError:
            n = None
            Message("⚠️ Number of Alternates must be integer!")

        if n:
            for k, val in self.alternatesDict.items():
                if strict is True and len(val) == n:
                    selectGlyphs.append(k)
                elif strict is False and len(val) >= n:
                    selectGlyphs.append(k)

        self.font.selection = []
        self.font.selection = [self.font.glyphs[gn] for gn in selectGlyphs]


if __name__ == '__main__':

    SelectAlternatesWindow()
