# MenuTitle: Select Glyphs Without Last Suffix

__doc__ = """
    Select any glyphs with a suffix (".something").
    Run the script:
    - It will deselect the current glyph and select the one without the (latest) extension if it exists:
        - /a.alt1 selected will deselect it and select /a
        - /a.ss01.alt1 selected will deselect it and select /a.ss01

    Usage:
    - Filter the alternate/name you want to select the parent and select all you want the parent selected (or manually select the alternates).
    - Run the script
    - It will select the "parent" glyphs.
    - Do what you want to do with the parents.
"""


gf = Glyphs.font
# gf.disableUpdateInterface()
Glyphs.clearLog()

parent_selection = []

for g in gf.selection:
    s = g.name.split('.')
    parent = ".".join(s[:-1])
    g.selected = False

    if gf.glyphs[parent]:
        parent_selection.append(gf.glyphs[parent])
        # gf.glyphs[parent].selected = True
    else:
        print(f"⚠️ /{g.name} doesn't have parent.")

gf.fontView.glyphSearchField().setStringValue_("")
gf.fontView.filterGlyphs_(None)
gf.fontView.glyphsArrayController().setSelectedObjects_(list(set(parent_selection)))

print("\nSelected:\n")
print(f"{' '.join([g.name for g in list(set(parent_selection))])}")

Glyphs.showMacroWindow()
# gf.enableUpdateInterface()
